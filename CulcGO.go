package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	var input string
	fmt.Println("Калькулятор 1.0\n" +
		"Калькулятор умеет выполнять операции сложения, вычитания, умножения и деления с двумя числами: a + b, a - b, a * b, a / b.\n" +
		"Вы можете работать с арабскими и римскими числами от 1 до 10 включительно.\n" +
		"\n\n" +
		"Введите выражение (например, 2 + 2):")
	reader := bufio.NewReader(os.Stdin)
	input, _ = reader.ReadString('\n')
	input = strings.TrimSpace(input)

	parts := strings.Fields(input)
	if len(parts) != 3 {
		panic("Ошибка: некорректный формат ввода")
	}

	num1, num2 := parts[0], parts[2]
	operator := parts[1]

	// Проверка наличия чисел в диапазоне от 1 до 10
	if !isValidInput(num1) || !isValidInput(num2) {
		panic("Ошибка: числа должны быть в диапазоне от 1 до 10")
	}

	arabic1, arabicErr1 := strconv.Atoi(num1)
	arabic2, arabicErr2 := strconv.Atoi(num2)

	if arabicErr1 == nil && arabicErr2 == nil {
		result := performOperation(arabic1, arabic2, operator)
		fmt.Println("Результат:", result)
	} else {
		roman1 := num1
		roman2 := num2
		arabic1, romanErr1 := romanToArabic(roman1)
		arabic2, romanErr2 := romanToArabic(roman2)

		if romanErr1 == nil && romanErr2 == nil {
			result := performOperation(arabic1, arabic2, operator)
			fmt.Println("Результат:", ArabicToRoman(result))
		} else {
			panic("Ошибка: введены разные типы чисел")
		}
	}
}

func isValidInput(num string) bool {
	if val, err := strconv.Atoi(num); err == nil {
		return val >= 1 && val <= 10
	} else {
		if _, err := romanToArabic(num); err == nil {
			arabicVal, _ := romanToArabic(num)
			return arabicVal >= 1 && arabicVal <= 10
		}
	}
	return false
}

func ArabicToRoman(number int) string {
	maxRomanNumber := 1000
	if number > maxRomanNumber {
		return strconv.Itoa(number)
	}

	conversions := []struct {
		value int
		digit string
	}{
		{1000, "M"},
		{900, "CM"},
		{500, "D"},
		{400, "CD"},
		{100, "C"},
		{90, "XC"},
		{50, "L"},
		{40, "XL"},
		{10, "X"},
		{9, "IX"},
		{5, "V"},
		{4, "IV"},
		{1, "I"},
	}

	var roman strings.Builder
	for _, conversion := range conversions {
		for number >= conversion.value {
			roman.WriteString(conversion.digit)
			number -= conversion.value
		}
	}

	return roman.String()
}

func romanToArabic(roman string) (int, error) {
	romanNumerals := map[rune]int{'I': 1, 'V': 5, 'X': 10}
	result := 0
	prev := 0

	for i := len(roman) - 1; i >= 0; i-- {
		value, exists := romanNumerals[rune(roman[i])]
		if !exists {
			return 0, fmt.Errorf("некорректное римское число")
		}
		if value < prev {
			result -= value
		} else {
			result += value
		}
		prev = value
	}
	return result, nil
}

func performOperation(num1, num2 int, operator string) int {
	switch operator {
	case "+":
		return num1 + num2
	case "-":
		return num1 - num2
	case "*":
		return num1 * num2
	case "/":
		if num2 == 0 {
			panic("Ошибка: деление на ноль")
		}
		return num1 / num2
	default:
		panic("Ошибка: некорректный оператор")
	}
}